# Tarea 1 - GitLab

<p align=left>
<a href="https://github.com/cifpfbmoll/tarea1-stonarini"><img src="https://img.shields.io/badge/GitHub-000000?style=for-the-badge&logo=github&logoColor=white"/></a>
<a href="https://bitbucket.org/stonarini/stonarini"><img src="https://img.shields.io/badge/Bitbucket-063985?style=for-the-badge&logo=bitbucket&logoColor=white"/></a>
</p>

## Create Repository
After [creating a GitLab account](https://gitlab.com/users/sign_up) and adding our [ssh key](https://docs.gitlab.com/ee/ssh/) to it, we can create a repository and start committing changes.

Firstly we click the '+' sign and then we select "new project/repository"

![](./img/new.png)

A new page will load with some option to choose from, we select "new project".

Then we will need to configure this new project (name, public/private, description, etc...);  
Be sure to initialize it with a README to be able to clone it later.

![](./img/config.png)

## Clone Repository
Once our repository has been created we can copy his url (or ssh
address) to clone it in our computer.

![](./img/clone-url.png)

With the address copied we open a terminal in the directory were we want to clone the repository and we execute:
```sh
$ git clone <repository-address>
```

![](./img/clone.png)

## Push to Repository
After cloning the repository we may change some files. How do we update the repository with our changes?  
We can update the repository by *committing* the changes and *pushing* them to the remote repository address with the following chain of commands:
```sh
$ git add <changed-files>
$ git commit -m <description of the changes>
$ git push
```

![](./img/push.png)

After pushing the changes, we can take a look at the repository in GitLab and we will see that our commit has been added and the repository is now updated.

![](./img/view-commit.png)