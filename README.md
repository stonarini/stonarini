# Tarea 2 - BitBucket

[![](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/stonarini/stonarini)

[![](https://img.shields.io/badge/GitHub-000000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/cifpfbmoll/tarea-2-si-stonarini)

## Create Repository
After [creating a BitBucket account](https://bitbucket.org/account/signup/) and adding our [ssh key](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/) to it, we can create a repository and start committing changes.

To create a repository, we go to our "repositories" page and then we click on "Create Repository" 
![](./img/new.png)

Then we will need to configure this new project (name, public/private, description, etc...);  
Be sure to initialize it with either a README (or a .gitignore) to be able to clone it later.

![](./img/config.png)

## Clone Repository
Once our repository has been created we can copy his url (or ssh
address) to clone it in our computer.

![](./img/clone-url.png)

With the address copied we open a terminal in the directory were we want to clone the repository and we execute:
```sh
$ git clone <repository-address>
```

![](./img/clone.png)

## Push to Repository
After cloning the repository we may change some files. How do we update the repository with our changes?  
We can update the repository by *committing* the changes and *pushing* them to the remote repository address with the following chain of commands:
```sh
$ git add <changed-files>
$ git commit -m <description of the changes>
$ git push
```

![](./img/push.png)

After pushing the changes, we can take a look at the repository in BitBucket and we will see that our commit has been added and the repository is now updated.

![](./img/view-commit.png)